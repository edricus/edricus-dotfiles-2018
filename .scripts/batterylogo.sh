#!/bin/bash
value=$(acpi | grep -Eo '[0-9]{1,4}%' | sed s/%//)
charging=$(acpi | grep -o Charging)
if [ "$charging" == 'Charging' ]; then
	echo 
fi
if [ $value == '100' ]; then
	echo ''
elif [ $value -lt '100' ] && [ $value -ge '75' ]; then
	echo ''
elif [ $value -lt '80' ] && [ $value -ge '50' ]; then
	echo ''
elif [ $value -lt '60' ] && [ $value -ge '35' ]; then
	echo ''
elif [ $value -lt '40' ] && [ $value -ge '10' ]; then
	echo ''
elif [ $value -lt '20' ] && [ $value -ge '10' ]; then
	echo ''
fi
