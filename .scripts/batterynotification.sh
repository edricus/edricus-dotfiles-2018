#!/bin/sh
value=$(acpi | grep Battery | cut -d " " -f4 | tr -d "%,")
status=$(acpi | grep Battery | cut -d " " -f3 | tr -d ",")
while [ true ]; do

if [ $status == "Charging" ] && [ $value == "100" ]; then
	exec $(notify-send -u critical "Batterie chargée à 100%! Veuillez déconnecter le chargeur.")
	fi

if [ $status == "Discharging" ] && [ $value -le "15" ]; then
	exec $(notify-send -u critical "Batterie faible! Veuillez connecter un chargeur.")
	fi

if [ $status == "Discharging" ] && [ $value -le "7" ]; then
	exec $(notify-send -u critical "La batterie est à un seuil critique! Extinction dans quelque minutes...")
	fi

if [ $status == "Discharging" ]  && [ $value -le "5" ]; then 
	exec $(i3lock --color 2f343f && systemctl suspend) 
	fi

sleep 2m
done
