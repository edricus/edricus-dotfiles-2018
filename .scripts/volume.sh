#!/bin/bash
state=$(amixer -c 0 get Master | grep Mono: | cut -d " " -f8 | tr -d "[]")
volume=$(amixer -c 0 get Master | grep Mono: | cut -d " " -f6 | tr -d "[%]")
set -x
if [ $state == "on" ] && [ $volume -ge 80 ]; then
	echo ""
elif [ $state == "on" ] && [ $volume -ge 50 ] && [ $volume -lt 80 ]; then
	echo ""
elif [ $state == "on" ] && [ $volume -gt 50 ]; then
	echo ""
else
	echo "muted"
fi
set +x
